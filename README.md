## JPA Criteria API(using h2 database- in-mem strategy)

```
refer - https://www.youtube.com/watch?v=ly8rdlapkgU&t=784s
```

### Way 1[NOT RECOMMENDED]

#### 1.1.1 AND/OR operators
```
[NOT RECOMMENDED]
As part of way1 we used boiler plate code to get result.

refer - StudentController 
                         -> getAndOperationByWay1(..)
                         -> getOrOperationByWay1(..)
                         -> getAndOrOperationCombinationByWay1(..)
```

#### 1.1.2 AND operators (in condition )
##### TODO

#### 1.1.3 AND/OR operators(using like for String and Number)
##### TODO

#### 1.2 Inner Joins(StudentCourse INNER JOIN Student INNER JOIN StudentAdditionalInfo)
```
[NOT RECOMMENDED]
As part of way1 we used boiler plate code to get result.

refer - StudentController 
                         -> getStudentCourseNameUsingJoinWay1(..)
```

#### 1.3 Projections Inner Joins(StudentCourse INNER JOIN Student INNER JOIN StudentAdditionalInfo) to get all the data from 3 tables
```
[NOT RECOMMENDED]
As part of way1 we used boiler plate code to get result.

refer - StudentController 
                         -> getProjectionUsingJoinWay1(..)
Note                         
1. refer - https://www.javatpoint.com/jpa-criteria-select-clause
2. as per the order of .multiselect(...) in below example 
                TypedQuery<StudentCourse> typedQuery = entityManager.createQuery(studentCourseQuery
                        .multiselect(
                                studentCourse.get("id"),studentCourse.get("name"), studentCourse.get("status"),
                                studentCourse.get("student"),
                                studentCourseStudentJoin.get("additionalInfo"))
                );
   define a constructor in target entity (i.e. StudentCourse in the above example.
   - refer 
     -- StudentCriteriaRepoImpl -> getProjectionUsingJoinWay1(..)
     -- StudentCourse -> StudentCourse(Long id,String name,StatusLookup status, Student student, StudentAdditionalInfo studentAdditionalInfo) 
```

#### 1.4 Pagination with Specification

##### TODO

### Way 2[RECOMMENDED OVER WAY1]

#### 2.1.1 AND/OR operators
```
[RECOMMENDED OVER WAY1]
As part of way 2 we removed boiler plate code by using Specification.

refer - StudentController 
                         -> getAndOperationByWay2(..)
                         -> getOrOperationByWay2(..)
                         -> getAndOrOperationCombinationByWay2(..)


Way 2 is **RECOMMENDED** approach over way1 as code length is reduced significantly if we use way2.

Note: If we use way2 then then repo should extend "JpaSpecificationExecutor<T>"
  - refer - com.poc.jpa.criteria.repo.StudentRepo
  else we will get wierd comppile time error when we call repo method on passing specification obj
```

#### 2.1.2 AND/OR operators(using in)

```
[RECOMMENDED OVER WAY1]
As part of way 2 we removed boiler plate code by using Specification.

refer - StudentController 
                         -> getInOperationByWay2(..)

Way 2 is **RECOMMENDED** approach over way1 as code length is reduced significantly if we use way2.

Note: If we use way2 then then repo should extend "JpaSpecificationExecutor<T>"
  - refer - com.poc.jpa.criteria.repo.StudentRepo
  else we will get wierd comppile time error when we call repo method on passing specification obj
```

#### 2.1.3 AND/OR operators(using like for String and Number)

```
[RECOMMENDED OVER WAY1]
As part of way 2 we removed boiler plate code by using Specification.

refer - StudentController 
                         -> getLikeStringOperationByWay2(..)
                         -> getLikeLongOperationByWay2(..)

Way 2 is **RECOMMENDED** approach over way1 as code length is reduced significantly if we use way2.

Note: If we use way2 then then repo should extend "JpaSpecificationExecutor<T>"
  - refer - com.poc.jpa.criteria.repo.StudentRepo
  else we will get wierd comppile time error when we call repo method on passing specification obj
```

#### 2.2 Inner Joins(StudentCourse INNER JOIN Student INNER JOIN StudentAdditionalInfo)
```
[RECOMMENDED OVER WAY1]
As part of way 2 we removed boiler plate code by using Specification.

refer - StudentController 
                         -> getStudentCourseNameUsingJoinWay2(..)


Way 2 is **RECOMMENDED** approach over way1 as code length is reduced significantly if we use way2.

Note: If we use way2 then then repo should extend "JpaSpecificationExecutor<T>"
  - refer - com.poc.jpa.criteria.repo.StudentCourseRepo
  else we will get wierd comppile time error when we call repo method on passing specification obj
```

#### [NOT WORKING]
##### this is a bug in Spring Data JPA - https://github.com/spring-projects/spring-data-jpa/issues/1524
##### track https://github.com/spring-projects/spring-data-jpa/issues/1524
#### 2.3 Projections Inner Joins(StudentCourse INNER JOIN Student INNER JOIN StudentAdditionalInfo) to get all the data from 3 tables
```
[RECOMMENDED OVER WAY1]
As part of way 2 we removed boiler plate code by using Specification.

refer - StudentController 
                         -> getProjectionUsingJoinWay2(..)
Note                         
1. refer - https://www.javatpoint.com/jpa-criteria-select-clause
2. as per the order of .multiselect(...) in below example 
                query.multiselect(
                    root.get("id"),root.get("name"), root.get("status"),
                    root.get("student"),
                    studentCourseStudentJoin.get("additionalInfo")
                    
   define a constructor in target entity (i.e. StudentCourse in the above example.
   - refer 
     -- StudentCourseSpecification -> getProjectionByStudent(..)
     -- StudentCourse -> StudentCourse(Long id,String name,StatusLookup status, Student student, StudentAdditionalInfo studentAdditionalInfo) 
```

#### 2.4 Pagination with Specification
##### TODO