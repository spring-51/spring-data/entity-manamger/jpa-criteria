package com.poc.jpa.criteria.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.poc.jpa.criteria.entities.Student;
import com.poc.jpa.criteria.entities.StudentAdditionalInfo;
import com.poc.jpa.criteria.entities.StudentCourse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class StudentCombinedResponse {
    // From Student entity
    private Long studentId;
    private String studentName;
    private String studentAddress;
    private String studentStatus;

    // From StudentAdditionalInfo entity
    private Long studentAdditionInfoId;
    private String studentMiddleName;
    private String studentLastName;
    private String studentAdditionInfoStatus;

    // From StudentCourse entity
    private Long studentCourseId;
    private String studentCourseName;
    private String studentCourseStatus;

    public static StudentCombinedResponse build(StudentCourse response) {
        Student student = response.getStudent();
        StudentAdditionalInfo additionalInfo = student.getAdditionalInfo();
        return builder()
                // From Student entity
                .studentId(student.getId())
                .studentName(student.getName())
                .studentAddress(student.getAddress())
                .studentStatus(student.getStatus().getId()+"-"+ student.getStatus().getStatus())

                // From StudentAdditionalInfo entity
                .studentAdditionInfoId(additionalInfo.getId())
                .studentAdditionInfoStatus(additionalInfo.getStatus().getId()+"-"+additionalInfo.getStatus().getStatus())
                .studentMiddleName(additionalInfo.getMiddleName())
                .studentLastName(additionalInfo.getLastName())

                // From StudentCourse entity
                .studentCourseId(response.getId())
                .studentCourseName(response.getName())
                .studentCourseStatus(response.getStatus().getId()+"-"+response.getStatus().getStatus())
                .build();
    }
}
