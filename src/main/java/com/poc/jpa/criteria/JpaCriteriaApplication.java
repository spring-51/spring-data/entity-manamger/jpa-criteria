package com.poc.jpa.criteria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaCriteriaApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaCriteriaApplication.class, args);
	}

}
