package com.poc.jpa.criteria.repo;

import com.poc.jpa.criteria.entities.Student;
import com.poc.jpa.criteria.response.StudentCombinedResponse;

import java.util.List;

public interface StudentCriteriaRepo {

    // [NOT-RECOMMENDED]
    // WAY 1 - starts
    List<Student> getAndOperationByWay1(String name, String address);

    List<Student> getOrOperationByWay1(String name, String address);

    List<Student> getAndOrOperationCombinationByWay1(String name, String address);

    String getStudentCourseNameUsingJoinWay1(Long studentId);

    StudentCombinedResponse getProjectionUsingJoinWay1(Long studentId);

    // WAY 1 - ends

    // [RECOMMENDED]
    // WAY 2 - Using Specification  - starts

    // WAY 2 - Using Specification  - ends
}
