package com.poc.jpa.criteria.repo;

import com.poc.jpa.criteria.entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepo extends StudentCriteriaRepo
        , JpaRepository<Student, Long>
        , JpaSpecificationExecutor<Student> // This is NEEDED if we use Specification for Spring data criteria APU
{
}
