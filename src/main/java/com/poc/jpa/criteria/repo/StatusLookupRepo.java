package com.poc.jpa.criteria.repo;

import com.poc.jpa.criteria.entities.StatusLookup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusLookupRepo extends JpaRepository<StatusLookup, Long> {
}
