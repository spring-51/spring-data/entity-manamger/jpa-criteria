package com.poc.jpa.criteria.repo;

import com.poc.jpa.criteria.entities.StatusLookup;
import com.poc.jpa.criteria.entities.Student;
import com.poc.jpa.criteria.entities.StudentAdditionalInfo;
import com.poc.jpa.criteria.entities.StudentCourse;
import com.poc.jpa.criteria.response.StudentCombinedResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentCriteriaRepoImpl implements StudentCriteriaRepo{

    @Autowired
    private EntityManager entityManager;

    // [NOT-RECOMMENDED]
    // WAY 1 - starts
    @Override
    public List<Student> getAndOperationByWay1(String name, String address) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Student> criteriaQuery = criteriaBuilder.createQuery(Student.class);
        Root<Student> student = criteriaQuery.from(Student.class);
        Predicate studentStatusPredicate = criteriaBuilder.equal(student.get("status"), new StatusLookup(StatusLookup.ACTIVE));
        Predicate studentNamePredicate = criteriaBuilder.equal(student.get("name"), name); // here "name" is as per field name of Student entity
                                                                                          // hibernate-jpamodelgen can be used to remove "name" hard coding BUT ITS NOT WORKING
        Predicate studentAddressPredicate = criteriaBuilder.like(student.get("address"), "%" + address + "%"); // here "address" is as per field name of Student entity
        criteriaQuery.where(studentStatusPredicate, studentNamePredicate, studentAddressPredicate);

        TypedQuery<Student> query = entityManager.createQuery(criteriaQuery);
        return query.getResultList();

    }

    @Override
    public List<Student> getOrOperationByWay1(String name, String address) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Student> criteriaQuery = criteriaBuilder.createQuery(Student.class);
        Root<Student> student = criteriaQuery.from(Student.class);
        Predicate studentStatusPredicate = criteriaBuilder.equal(student.get("status"), new StatusLookup(StatusLookup.ACTIVE));
        Predicate studentNamePredicate = criteriaBuilder.equal(student.get("name"), name); // here "name" is as per field name of Student entity
                                                                                           // hibernate-jpamodelgen can be used to remove "name" hard coding BUT ITS NOT WORKING
        Predicate studentAddressPredicate = criteriaBuilder.like(student.get("address"), "%" + address + "%"); // here "address" is as per field name of Student entity
        Predicate orPredicate = criteriaBuilder.or(studentStatusPredicate, studentNamePredicate, studentAddressPredicate);
        criteriaQuery.where(orPredicate);
        TypedQuery<Student> query = entityManager.createQuery(criteriaQuery);
        return query.getResultList();

    }

    @Override
    public List<Student> getAndOrOperationCombinationByWay1(String name, String address) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Student> criteriaQuery = criteriaBuilder.createQuery(Student.class);
        Root<Student> student = criteriaQuery.from(Student.class);

        Predicate studentStatusPredicate = criteriaBuilder.equal(student.get("status"), new StatusLookup(StatusLookup.ACTIVE));

        Predicate studentNamePredicate1 = criteriaBuilder.equal(student.get("name"), name);
        Predicate studentNamePredicate2 = criteriaBuilder.equal(student.get("name"), "name2");
        Predicate orNamePredicate = criteriaBuilder.or(studentNamePredicate1, studentNamePredicate2);

        Predicate studentAddressPredicate1 = criteriaBuilder.like(student.get("address"), "%" + address + "%"); // here "address" is as per field name of Student entity
        Predicate studentAddressPredicate2 = criteriaBuilder.equal(student.get("address"), "address2");
        Predicate orAddressPredicate = criteriaBuilder.or(studentAddressPredicate1, studentAddressPredicate2);

        Predicate orAndCombinationPredicate = criteriaBuilder.and(studentStatusPredicate,orNamePredicate, orAddressPredicate);

        criteriaQuery.where(orAndCombinationPredicate);
        TypedQuery<Student> query = entityManager.createQuery(criteriaQuery);
        return query.getResultList();

    }

    // refer - https://stackoverflow.com/questions/17154676/criteria-jpa-2-with-3-tables
    @Override
    public String getStudentCourseNameUsingJoinWay1(Long studentId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<StudentCourse> studentCourseQuery = criteriaBuilder.createQuery(StudentCourse.class);
        Root<StudentCourse> studentCourse = studentCourseQuery.from(StudentCourse.class);
        // Inner Join  StudentCourse to Student
        Join<StudentCourse, Student> studentCourseStudentJoin = studentCourse.join("student");
        // Inner Join  Student to StudentAdditionalInfo
        Join<Student, StudentAdditionalInfo> studentStudentAdditionalInfoJoin = studentCourseStudentJoin.join("additionalInfo");

        // WHERE clause on StudentCourse
        Predicate hasStudentCourseStatusId = criteriaBuilder.equal(studentCourse.get("status"), new StatusLookup(StatusLookup.ACTIVE));
        // WHERE clause on Student
        Predicate hasStudentId = criteriaBuilder.equal(studentCourseStudentJoin.get("id"), studentId);
        Predicate hasStudentStatusId = criteriaBuilder.equal(studentCourseStudentJoin.get("status"), new StatusLookup(StatusLookup.ACTIVE));
        // WHERE clause on StudentAdditionalInfo
        Predicate hasStudentAdditionalInfoStatusId = criteriaBuilder.equal(studentStudentAdditionalInfoJoin.get("status"), new StatusLookup(StatusLookup.ACTIVE));

        studentCourseQuery.where(hasStudentId, hasStudentStatusId, hasStudentCourseStatusId, hasStudentAdditionalInfoStatusId);
        TypedQuery<StudentCourse> typedQuery = entityManager.createQuery(studentCourseQuery);

        //[WORKING]
        // below is alternative approach to instantiate TypedQuery<StudentCourse>
        /*
        TypedQuery<StudentCourse> typedQuery = entityManager.createQuery(studentCourseQuery
                        .select(fromStudentCourse)
                .where(hasStudentId, hasStudentStatusId, hasStudentCourseStatusId)
                .orderBy(criteriaBuilder.asc(fromStudentCourse.get("name")))
                .distinct(true)
        );
         */
        List<StudentCourse> resultList = typedQuery.getResultList();
        if(ObjectUtils.isEmpty(resultList)){
            return "no record found";
        }

        return resultList.get(0).getName();
    }

    @Override
    public StudentCombinedResponse getProjectionUsingJoinWay1(Long studentId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<StudentCourse> studentCourseQuery = criteriaBuilder.createQuery(StudentCourse.class);
        Root<StudentCourse> studentCourse = studentCourseQuery.from(StudentCourse.class);
        // Inner Join  StudentCourse to Student
        Join<StudentCourse, Student> studentCourseStudentJoin = studentCourse.join("student");
        // Inner Join  Student to StudentAdditionalInfo
        Join<Student, StudentAdditionalInfo> studentStudentAdditionalInfoJoin = studentCourseStudentJoin.join("additionalInfo");

        // WHERE clause on StudentCourse
        Predicate hasStudentCourseStatusId = criteriaBuilder.equal(studentCourse.get("status"), new StatusLookup(StatusLookup.ACTIVE));
        // WHERE clause on Student
        Predicate hasStudentId = criteriaBuilder.equal(studentCourseStudentJoin.get("id"), studentId);
        Predicate hasStudentStatusId = criteriaBuilder.equal(studentCourseStudentJoin.get("status"), new StatusLookup(StatusLookup.ACTIVE));
        // WHERE clause on StudentAdditionalInfo
        Predicate hasStudentAdditionalInfoStatusId = criteriaBuilder.equal(studentStudentAdditionalInfoJoin.get("status"), new StatusLookup(StatusLookup.ACTIVE));

        // [WORKING]
        // **commented** as its defined while instantiating "typedQuery" below
        // studentCourseQuery.where(hasStudentId, hasStudentStatusId, hasStudentCourseStatusId, hasStudentAdditionalInfoStatusId);
        // refer - https://www.javatpoint.com/jpa-criteria-select-clause
        TypedQuery<StudentCourse> typedQuery = entityManager.createQuery(studentCourseQuery
                .multiselect(
                        studentCourse.get("id"),studentCourse.get("name"), studentCourse.get("status"),
                        studentCourse.get("student"),
                        studentCourseStudentJoin.get("additionalInfo"))
                 .where(
                         hasStudentId, hasStudentStatusId,
                         hasStudentCourseStatusId, hasStudentAdditionalInfoStatusId)
        );

        List<StudentCourse> resultList = typedQuery.getResultList();
        if(ObjectUtils.isEmpty(resultList)){
            return new StudentCombinedResponse();
        }

        StudentCombinedResponse response = StudentCombinedResponse.build(resultList.get(0));
        return response;
    }

    // WAY 1 ends
    // [RECOMMENDED]
    // WAY 2 - Using Specification  - starts


    // WAY 2 - Using Specification  - ends
}
