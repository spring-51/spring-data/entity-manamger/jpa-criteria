package com.poc.jpa.criteria.repo;

import com.poc.jpa.criteria.entities.StudentAdditionalInfo;
import jdk.jfr.Registered;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentAdditionalInfoRepo extends JpaRepository<StudentAdditionalInfo, Long> {
}
