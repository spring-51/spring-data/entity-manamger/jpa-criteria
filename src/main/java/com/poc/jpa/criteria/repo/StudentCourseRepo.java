package com.poc.jpa.criteria.repo;

import com.poc.jpa.criteria.entities.Student;
import com.poc.jpa.criteria.entities.StudentCourse;
import org.hibernate.cfg.JPAIndexHolder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentCourseRepo extends JpaRepository<StudentCourse, Long>, JpaSpecificationExecutor<StudentCourse> {
}
