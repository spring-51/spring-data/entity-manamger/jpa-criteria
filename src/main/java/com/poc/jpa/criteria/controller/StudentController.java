package com.poc.jpa.criteria.controller;

import com.poc.jpa.criteria.entities.StatusLookup;
import com.poc.jpa.criteria.entities.Student;
import com.poc.jpa.criteria.entities.StudentCourse;
import com.poc.jpa.criteria.repo.StudentCourseRepo;
import com.poc.jpa.criteria.repo.StudentCriteriaRepo;
import com.poc.jpa.criteria.repo.StudentRepo;
import static com.poc.jpa.criteria.specification.StudentSpecification.*;

import static com.poc.jpa.criteria.specification.StudentCourseSpecification.*;

import com.poc.jpa.criteria.response.StudentCombinedResponse;
import com.poc.jpa.criteria.specification.StudentCourseSpecification;
import com.poc.jpa.criteria.specification.StudentSpecification;
import lombok.RequiredArgsConstructor;
import static org.springframework.data.jpa.domain.Specification.*;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentRepo repo;
    private final StudentCourseRepo studentCourseRepo;
    // [NOT-RECOMMENDED]
    // WAY 1 - starts
    @GetMapping("/and/way1")
    public List<Student> getAndOperationByWay1(@RequestParam(value = "n", defaultValue = "name1") String name,
                                   @RequestParam(value = "a", defaultValue = "address1") String address){
        return repo.getAndOperationByWay1(name, address);
    }

    @GetMapping("/or/way1")
    public List<Student> getOrOperationByWay1(@RequestParam(value = "n", defaultValue = "name1") String name,
                                               @RequestParam(value = "a", defaultValue = "address1") String address){
        return repo.getOrOperationByWay1(name, address);
    }

    @GetMapping("/and-or/way1")
    public List<Student> getAndOrOperationCombinationByWay1(@RequestParam(value = "n", defaultValue = "name1") String name,
                                              @RequestParam(value = "a", defaultValue = "address1") String address){
        return repo.getAndOrOperationCombinationByWay1(name, address);
    }

    @GetMapping("/join/way1")
    public String getStudentCourseNameUsingJoinWay1(@RequestParam(name = "sId", defaultValue = "1")Long studentId){
        return repo.getStudentCourseNameUsingJoinWay1(studentId);
    }

    @GetMapping("/projection/join/way1")
    public StudentCombinedResponse getProjectionUsingJoinWay1(@RequestParam(name = "sId", defaultValue = "1")Long studentId){
        return repo.getProjectionUsingJoinWay1(studentId);
    }
    // WAY 1 - ends

    // [RECOMMENDED]
    // WAY 2 - Using Specification  - starts

    @GetMapping("/and/way2")
    public List<Student> getAndOperationByWay2(@RequestParam(value = "n", defaultValue = "name1") String name,
                                               @RequestParam(value = "a", defaultValue = "address1") String address){
        var specification =where(StudentSpecification.hasStatus(StatusLookup.ACTIVE).and(hasName(name)).and(containsAddress(address)));
        return repo.findAll(specification);
    }

    @GetMapping("/or/way2")
    public List<Student> getOrOperationByWay2(@RequestParam(value = "n", defaultValue = "name1") String name,
                                              @RequestParam(value = "a", defaultValue = "address1") String address){
        var specification =where(StudentSpecification.hasStatus(StatusLookup.ACTIVE).or(hasName(name)).or(containsAddress(address)));
        return repo.findAll(specification);
    }

    @GetMapping("/and-or/way2")
    public List<Student> getAndOrOperationCombinationByWay2(@RequestParam(value = "n", defaultValue = "name1") String name,
                                                            @RequestParam(value = "a", defaultValue = "address1") String address){
        var orNameSpecification =where(hasName(name).or(hasName("name2")));
        var orAddressSpecification =where(containsAddress(address).or(hasAddress("address2")));

        var specification = where(StudentSpecification.hasStatus(StatusLookup.ACTIVE).and(orNameSpecification).and(orAddressSpecification));
        return repo.findAll(specification);
    }

    @GetMapping("/and/in/way2")
    public List<Student> getInOperationByWay2(@RequestParam(value = "n", defaultValue = "name1,name2") List<String> name){
        var hasNamesSpecification =where(hasNames(name));
        var specification = hasNamesSpecification.and(StudentSpecification.hasStatus(StatusLookup.ACTIVE));
        return repo.findAll(specification);
    }

    @GetMapping("/and/likeString/way2")
    public List<Student> getLikeStringOperationByWay2(@RequestParam(value = "str", required = false, defaultValue = "a") String str){
        var hasNamesSpecification =where(containsAddress(str));
        var specification = hasNamesSpecification.and(StudentSpecification.hasStatus(StatusLookup.ACTIVE));
        return repo.findAll(specification);
    }

    @GetMapping("/and/likeLong/way2")
    public List<Student> getLikeLongOperationByWay2(@RequestParam(value = "str", required = false,defaultValue = "1") Long str){
        var hasNamesSpecification =where(containsStudentId(str));
        var specification = hasNamesSpecification.and(StudentSpecification.hasStatus(StatusLookup.ACTIVE));
        return repo.findAll(specification);
    }

    @GetMapping("/join/way2")
    public String getStudentCourseNameUsingJoinWay2(@RequestParam(name = "sId", defaultValue = "1")Long studentId){

        var joinStudentSpecification = where(StudentCourseSpecification.hasStatus(StatusLookup.ACTIVE).and(getStudentCourseByStudent(studentId)));
        List<StudentCourse> studentCourses = studentCourseRepo.findAll(joinStudentSpecification);
        if(ObjectUtils.isEmpty(studentCourses)){
            return "no record found";
        }
        return studentCourses.get(0).getName();
    }

    @GetMapping("/projection/join/way2")
    public StudentCombinedResponse getProjectionUsingJoinWay2(@RequestParam(name = "sId", defaultValue = "1")Long studentId){

        var joinStudentSpecification = where(StudentCourseSpecification.hasStatus(StatusLookup.ACTIVE).and(getProjectionByStudent(studentId)));
        List<StudentCourse> studentCourses = studentCourseRepo.findAll(joinStudentSpecification);
        if(ObjectUtils.isEmpty(studentCourses)){
            return new StudentCombinedResponse();
        }
        StudentCombinedResponse response = StudentCombinedResponse.build(studentCourses.get(0));
        return response;
    }

    // WAY 2 - Using Specification  - ends
}
