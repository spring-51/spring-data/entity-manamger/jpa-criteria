package com.poc.jpa.criteria.bootstrap;

import com.poc.jpa.criteria.entities.StatusLookup;
import com.poc.jpa.criteria.entities.Student;
import com.poc.jpa.criteria.entities.StudentAdditionalInfo;
import com.poc.jpa.criteria.entities.StudentCourse;
import com.poc.jpa.criteria.repo.StatusLookupRepo;
import com.poc.jpa.criteria.repo.StudentAdditionalInfoRepo;
import com.poc.jpa.criteria.repo.StudentCourseRepo;
import com.poc.jpa.criteria.repo.StudentRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {

    private final StudentRepo repo;

    private final StatusLookupRepo statusLookupRepo;

    private final StudentCourseRepo studentCourseRepo;

    private final StudentAdditionalInfoRepo studentAdditionalInfoRepo;

    @Override
    public void run(String... args) throws Exception {
        StatusLookup status = statusLookupRepo.save(StatusLookup.builder().id(StatusLookup.ACTIVE).status("ACTIVE").build());

        List<Student> students = repo.saveAll(List.of(
                Student.builder().name("name1").address("address1").status(status).build(),
                Student.builder().name("name2").address("address2").status(status).build(),
                Student.builder().name("name3").address("address3").status(status).build()
        ));

        int count = 1;
        for (Student student : students) {
            studentCourseRepo.save(StudentCourse.builder()
                    .student(student)
                    .name("course"+count++)
                    .status(status).build()
            );
            StudentAdditionalInfo studAddInfo = studentAdditionalInfoRepo.save(StudentAdditionalInfo.builder()
                    .lastName(student.getName() + "-lastname")
                    .middleName(student.getName() + "-middlename")
                    .status(status)
                    .build()
            );

            student.setAdditionalInfo(studAddInfo);
            repo.save(student);

        }
    }
}
