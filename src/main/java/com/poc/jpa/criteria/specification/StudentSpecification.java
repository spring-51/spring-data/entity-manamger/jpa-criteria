package com.poc.jpa.criteria.specification;

import com.poc.jpa.criteria.entities.StatusLookup;
import com.poc.jpa.criteria.entities.Student;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public class StudentSpecification {

    public static Specification<Student> hasStatus(Long statusId){
        Specification<Student> spec = (student, cq, cb) -> cb.equal(student.get("status"), new StatusLookup(statusId));
        return spec;
    }

    public static Specification<Student> hasName(String name){
        Specification<Student> spec = (student, cq, cb) -> cb.equal(student.get("name"), name);
        return spec;
    }

    public static Specification<Student> hasNames(List<String> names){
        Specification<Student> spec = (student, cq, cb) -> cb.in(student.get("name")).value(names);
        return spec;
    }

    public static Specification<Student> containsAddress(String address){
        Specification<Student> spec = (student, cq, cb) -> cb.like(student.get("address"), "%"+address+"%");
        return spec;
    }

    public static Specification<Student> containsStudentId(Long studentId){
        Specification<Student> spec = (student, cq, cb) -> cb.like(student.get("id").as(String.class), "%"+studentId+"%");
        return spec;
    }

    public static Specification<Student> hasAddress(String address){
        Specification<Student> spec = (student, cq, cb) -> cb.equal(student.get("address"), address);
        return spec;
    }
}
