package com.poc.jpa.criteria.specification;

import com.poc.jpa.criteria.entities.StatusLookup;
import com.poc.jpa.criteria.entities.Student;
import com.poc.jpa.criteria.entities.StudentAdditionalInfo;
import com.poc.jpa.criteria.entities.StudentCourse;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;

public class StudentCourseSpecification {
    public static Specification<StudentCourse> hasStatus(Long statusId) {
        return (root, query, criteriaBuilder) -> {
            Predicate equalPredicate = criteriaBuilder.equal(root.get("status"), new StatusLookup(statusId));
            return equalPredicate;
        };
    }

    public static Specification<StudentCourse> getStudentCourseByStudent(Long studentId) {
        return (root, query, criteriaBuilder) -> {
            // Inner Join  StudentCourse to Student
            Join<Object, Object> studentCourseStudentJoin = root.join("student");
            // Inner Join  StudentCourse to Student
            var studentStudentAdditionalInfoJoin = studentCourseStudentJoin.join("additionalInfo");

            // WHERE clause on Student
            Predicate studentIdPredicate = criteriaBuilder.equal(studentCourseStudentJoin.get("id"), studentId);
            Predicate studentStatusPredicate = criteriaBuilder.equal(studentCourseStudentJoin.get("status"), new StatusLookup(StatusLookup.ACTIVE));
            // WHERE clause on StudentAdditionalInfo
            Predicate studentAdditionalInfoStatusPredicate = criteriaBuilder.equal(studentStudentAdditionalInfoJoin.get("status"), new StatusLookup(StatusLookup.ACTIVE));

            // query.distinct(true);
            return criteriaBuilder.and(studentIdPredicate,studentStatusPredicate,studentAdditionalInfoStatusPredicate);
        };
    }

    public static Specification<StudentCourse> getProjectionByStudent(Long studentId) {
        return (root, query, criteriaBuilder) -> {
            // Inner Join  StudentCourse to Student
            Join<Object, Object> studentCourseStudentJoin = root.join("student");
            // Inner Join  StudentCourse to Student
            var studentStudentAdditionalInfoJoin = studentCourseStudentJoin.join("additionalInfo");

            // WHERE clause on Student
            Predicate studentIdPredicate = criteriaBuilder.equal(studentCourseStudentJoin.get("id"), studentId);
            Predicate studentStatusPredicate = criteriaBuilder.equal(studentCourseStudentJoin.get("status"), new StatusLookup(StatusLookup.ACTIVE));
            // WHERE clause on StudentAdditionalInfo
            Predicate studentAdditionalInfoStatusPredicate = criteriaBuilder.equal(studentStudentAdditionalInfoJoin.get("status"), new StatusLookup(StatusLookup.ACTIVE));

            //[NOT WORKING]
            // this is a bug in Spring Data JPA - https://github.com/spring-projects/spring-data-jpa/issues/1524
            // track https://github.com/spring-projects/spring-data-jpa/issues/1524
            return query.multiselect(
                    root.get("id"),root.get("name"), root.get("status"),
                    root.get("student"),
                    studentCourseStudentJoin.get("additionalInfo")
            ).where(criteriaBuilder.and(studentIdPredicate,studentStatusPredicate,studentAdditionalInfoStatusPredicate)).getRestriction();

        };
    }


}
