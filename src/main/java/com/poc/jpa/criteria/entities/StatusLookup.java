package com.poc.jpa.criteria.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class StatusLookup {
    public static final Long ACTIVE = 1l;
    @Id
    @Builder.Default
    private Long id = ACTIVE;
    private String status;

    public StatusLookup(Long id) {
        this.id = id;
    }
}
