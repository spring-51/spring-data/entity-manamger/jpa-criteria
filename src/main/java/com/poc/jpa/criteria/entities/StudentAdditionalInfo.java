package com.poc.jpa.criteria.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class StudentAdditionalInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String middleName;
    private String lastName;
    @JsonIgnore // This is to avoid transaction exception since we are returning entity from controller
                // SerializationFeature.FAIL_ON_EMPTY_BEANS
                // Since we have marked fetch as LAZY, if we mark it EAGER then no need of @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_id")
    private StatusLookup status;
}
