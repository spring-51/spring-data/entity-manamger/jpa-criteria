package com.poc.jpa.criteria.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class StudentCourse {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_id")
    private StatusLookup status = new StatusLookup();

    @OneToOne
    @JoinColumn(name = "student_id")
    private Student student;

    /**
     * Constructor for Projections
     * way 1 - refer - StudentCriteriaRepoImpl -> getProjectionUsingJoinWay1(..)
     * way 2 - refer - StudentCourseSpecification -> getProjectionByStudent(..)
     * @param name
     * @param student
     */
    public StudentCourse(Long id,String name,StatusLookup status, Student student, StudentAdditionalInfo studentAdditionalInfo) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.student = student;
        this.student.setAdditionalInfo(studentAdditionalInfo);
    }
}
